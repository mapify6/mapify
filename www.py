import subprocess
import requests
import urllib2
from flask import Flask, render_template, request, g, redirect, url_for
from parsing.handle_html import handle_html
from index.search import *
from celery import Celery
import os.path
import json
from tasks import *
import math
from numpy import median

#celery = Celery('tasks',backend="amqp",  broker='amqp://lsir-cluster-01.epfl.ch:5672/')


index_searcher = Searcher()

app = Flask(__name__)


def compute_center(data):
  lats = []
  lons = []
  for result in data:
    if math.isnan(result['Place']['lat']) or math.isnan(result['Place']['lon']):
	continue
    lats.append(result['Place']['lat'])
    lons.append(result['Place']['lon'])
  return median(lats), median(lons)
 
@app.route('/')
@app.route('/index')
def homepage():
    print "Rendering Homepage"
    return render_template('bootstrap/index2.html')

def format_url(url):
    ans = url
    if not url.startswith("http"):
        ans = "http://" + ans
    return ans    

def download_url_direct(url):
    url = format_url(url)
    print "Retrieving URL-->" + url + "<---"
    r = requests.get(url)
    if r.status_code != 200:
        print "Error in downloading URL" +  str(url)
    else:
        return r.text    
  
  
def get_google_results(query,debug=True):
    print "Getting Google Results"
   # key='AIzaSyBN03AqxrylIM43SAR78Yhar_T5Pi8FcdE'
    key = 'AIzaSyBnSRQxvQo44yRL6hydxuI3nYU-l7BFhLM'
    query = "_".join(query.split())
    path = "google_results/" + query
    if os.path.isfile(path):
        with open(path) as fp:
	    json_data = json.load(fp)
            return json_data
    
    if debug:
        print "Fetching query -->",query
    data = urllib2.urlopen('https://www.googleapis.com/customsearch/v1?key=' + key + '&cx=002395991479798109679:3zgu7hsltbu&q=' + urllib2.quote(query))
    json_data = json.load(data)
    with open(path,"w") as fp:
        json.dump(json_data,fp)
        
        
    return json_data  
        

def get_urls(jsonv):
    ans = []
    if 'items' in jsonv:
        ans = [x['link'] for x in jsonv['items'] if 'link' in x]
   
    
    if len(ans) > 5:
        ans = ans[0:5]
        
    print "Found " + str(len(ans)) + " results"
    return ans


def download_urls(list_url):
     print "Downloading URLs"
     res = []
     for url in list_url:
        res.append(download_url.delay(url))
     
     ans = []
     for x in res:
         x.wait()
         ans.append(x.get())
     
     return ans    
"""
Fetches the page provided and gives it forward for parsing.

"""


def remove_duplicates(data):
      uniq_dict = {}
      ans = []
      for x in data['data']:
      	  dictv = x['Place']
          print dictv
          str_desc = (str(dictv['city'].encode('utf-8')) + "___" + str(dictv['postcode'].encode('utf-8')) + "___" + str(dictv['road'].encode('utf-8')) + "___" + str(dictv['housenumber'].encode('utf-8'))).lower() 
          print "AMIT AMIT AMIT ",str_desc 
          if str_desc in uniq_dict:
	      continue
          uniq_dict[str_desc] = 1
          ans.append(x)
      data['data'] = ans
      return data
         

def combine_url_results(ur):
    center_x = 0
    center_y = 0
    count = 0
    results = []
    
    for x in ur:
        if math.isnan(x['center']['lat']) or math.isnan(x['center']['lon']):
	    continue
        results = results + x['data']
        n_r = len(x['data'])
        print n_r, center_x, center_y    
        center_x = center_x + n_r * x['center']['lat']
        center_y = center_y + n_r * x['center']['lon']
        count = count + n_r
        
    print "final",count,center_x,center_y
    ans = {}
    ans['center'] = {}
    x,y = compute_center(results) 
    ans['center']['lat'] = x 
    ans['center']['lon'] = y 
    ans['data'] = results
    
    return ans

@app.route('/parse/', methods=['POST','GET'])
def parse():
    fwout = open("log.txt","w")
    print "Inside parsing routine"
    url = request.form['url']
    print "URL --> " , url
    fwout.write("AMIT AMIT AMIT\n")
    results = {}
    if url.startswith("http") or url.startswith("www"):
	url_html = download_url_direct(url)
        results = find_information_from_html(url_html)
 	for x in results['data']:
            x['url'] = url
            x['short_url'] = url[0:25] + "..."
    else: 
    	gr = get_google_results(url)
    	urls = get_urls(gr)
    	url_htmls = download_urls(urls)
    	url_results = []
        fwout.write(str(urls) + "\n")
    	for i,uh in enumerate(url_htmls):
            if (len(uh)) > 500000:
		continue
            listv = find_information_from_html(uh)
 	    for x in listv['data']:
            	x['url'] = urls[i]
	        x['short_url'] = urls[i][0:25] + "..."    
            url_results.append(listv)
             
            fwout.write("Len -->" + str(i) + "," + str(len(uh)) + "\n")
	    fwout.write(str(i) + "    " + str(listv['data']) + "\n" )     
        results = combine_url_results(url_results)   
    #return url + "--------------" + json
    #json = []
    #json.append({"lat":46.523,"lon":6.586})
    #json.append({"lat":46.5203,"lon":6.5656})
    results = remove_duplicates(results)
    return render_template("bootstrap/results.html",results=results)

def find_information_from_html(html):
    json = handle_html(html,index_searcher)
    return json

#@app.context_processor
#def inject_places(results):
#    return dict(results=results)

if __name__ == '__main__':
    print "Starting App"
    app.run(host="0.0.0.0",debug=True)
