INSERT INTO StaticKeywords(keyword, type, language) values ('Switzerland', 'country', 'english');
INSERT INTO StaticKeywords(keyword, type, language) values ('Suisse', 'country', 'french');
INSERT INTO StaticKeywords(keyword, type, language) values ('Schweiz', 'country', 'german');
INSERT INTO StaticKeywords(keyword, type, language) values ('Svizzera', 'country', 'italian');
INSERT INTO StaticKeywords(keyword, type, language) values ('Tel', 'phone_descriptor', 'english');
INSERT INTO StaticKeywords(keyword, type, language) values ('Phone', 'phone_descriptor', 'english');
INSERT INTO StaticKeywords(keyword, type, language) values ('Phone No', 'phone_descriptor', 'english');
INSERT INTO StaticKeywords(keyword, type, language) values ('Phone number', 'phone_descriptor', 'english');
