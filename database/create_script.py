import sys
import os

def create_script(input_file):
	input_path = input_file 
	output_path = input_file.split('.')[0]+"_script.sql"
	bad_path = input_file.split('.')[0]+"_bad.txt"

	input = open(input_path, 'r')
	output = open(output_path, 'w')
	bad_input = open(bad_path, 'w')
	i = 0
	statement = "INSERT INTO "

	for line in input:
		if i==0: 
			table_name = line.rstrip()
		elif i==1: 
			columns = line.rstrip().lstrip('\t')
			cols = columns.split(',')
		else: 
			values = line.rstrip()
			vals = values.split(', ')
			statement = statement + table_name + "(" + columns + ") values (" 
			if len(vals) == len(cols):
				j = 0
				for v in vals[:]:
					j=j+1
					if v.isdigit():
						v=v
					else:									 
						v="\'"+v.replace("\'","\'\'")+ "\'"
					statement = statement + v;
					if (j < len(vals)):
						statement = statement + ", "
				statement = statement + ");\n" 
				output.write(statement)
				statement = "INSERT INTO " 
			else:
				bad_input.write(line)
				statement = "INSERT INTO "
		i=i+1 
	
	bad_input.close()	
	input.close()
	output.close()

	if os.stat(bad_path)[6] == 0:
		os.remove(bad_path)
	return
	
create_script(sys.argv[1])