create sequence statickeywords_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 10;

CREATE TABLE StaticKeywords(
id integer primary key DEFAULT nextval('statickeywords_seq'),
keyword varchar(60),
type varchar(20),
language varchar(30),
CONSTRAINT keyword_unique UNIQUE(keyword, type, language));

create sequence regexp_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 10;

CREATE TABLE RegularExpressions (
id integer primary key DEFAULT nextval('regexp_seq'),
reg_ex varchar(250),
type varchar(60), 
language varchar(30),
CONSTRAINT id_reg_ex_unique UNIQUE(reg_ex, type, language)
);

create sequence places_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 10;
	
CREATE TABLE Places (
id integer primary key DEFAULT nextval('places_seq'),
name varchar(60),
lat decimal(9,5) ,
long decimal(9,5), 
description varchar(200),
rating integer, 
CONSTRAINT rating_max CHECK (rating >=0 and rating <=10), 
CONSTRAINT coord_unique UNIQUE (lat, long)
);

 create sequence holidays_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 10;
	
CREATE TABLE Holidays (
id integer primary key DEFAULT nextval('holidays_seq'),
name varchar(60),
date date, 
language varchar(60),
CONSTRAINT holiday_unique UNIQUE(name,DATE, language));

create sequence timeperiods_seq
    start 1
    increment 1
    NO MAXVALUE
    CACHE 10;
CREATE TABLE TimePeriods (
id integer primary key DEFAULT nextval('timeperiods_seq'),
open_hour time,
close_hour time);

CREATE TABLE HolidayHours (
place_id integer references Places(id), 
holiday_id integer references Holidays(id), 
timeperiod_id integer references TimePeriods(id),
CONSTRAINT HOLIDAYHOURS_UNIQUE UNIQUE(place_id, holiday_id,timeperiod_id)
);


CREATE TABLE WeekHours(
place_id integer references Places(id), 
day_id integer references WeekDays(day_id),
timeperiod_id integer references TimePeriods(id),
CONSTRAINT weekhours_unique UNIQUE(place_id, day_id, timeperiod_id)
);

Create table WeekDays(
day_id integer primary key,
day varchar(30)
);

CREATE TABLE WeekDays_ml(
day_id integer references WeekDays,
language varchar(30),
day varchar(20),
CONSTRAINT weekday_pk PRIMARY KEY(day_id, language) );
