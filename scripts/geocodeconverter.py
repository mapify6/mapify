#!/usr/bin/python

import math

originShift = 2 * math.pi * 6378137 / 2.0

"""
Taken from http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/
"""
def MetersToLatLon(mx, my ):
    "Converts XY point from Spherical Mercator EPSG:900913 to lat/lon in WGS84 Datum"

    lon = (mx / originShift) * 180.0
    lat = (my / originShift) * 180.0

    lat = 180 / math.pi * (2 * math.atan( math.exp( lat * math.pi / 180.0)) - math.pi / 2.0)
    return lat, lon