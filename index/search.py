# -*- coding: CP1251 -*-

import os
import logging
import re

logging.basicConfig(level=logging.DEBUG)

class Searcher:
    def __init__(self):
        self.word_positions = dict()
        self.word_id = dict()
        self.Load()

    def Search(self, query):
        invertIndex = open(os.path.join(os.path.dirname(__file__), "doc/InvertedIndex_0.txt"))
        words = re.split("-|'|,|;| |\t|&", query)
        words = " ".join(words).split()
        result_for_one_word = []
        for word in words:
            if word not in ['s']:
                try:
                    w_id = self.word_id[word]
                except:
                    return set()
                if self.word_positions.has_key(w_id):
                    invertIndex.seek(self.word_positions[w_id])
                    doc_index = invertIndex.readline()
                    node_index = [i for i in doc_index.split()[::4]]
                    name_object = [i for i in doc_index.split()[1::4]]
                    priority = [i for i in doc_index.split()[2::4]]
                    is_it_way = [i for i in doc_index.split()[3::4]]
                    result_for_one_word.append(set(zip(node_index, name_object, priority, is_it_way)))

        invertIndex.close()
        set_result = reduce(lambda x, y: x.intersection(y), result_for_one_word)
        result = [(node_id, name, priority, is_it_way) for (node_id, name, priority, is_it_way) in set_result]
        result = sorted(result, key=lambda r: r[2])
        return result#reduce(lambda x, y: x.intersection(y), result_for_one_word)



    def Load(self):
        word_wp_num = open(os.path.join(os.path.dirname(__file__), "doc/WP_0.txt"))
        word_id_file = open(os.path.join(os.path.dirname(__file__), "word_id.txt"))

        logging.debug("Loading...")

        idf = {}


        for line in word_wp_num:
            w_id, self.word_positions[w_id] = line.split()
            self.word_positions[w_id] = int(self.word_positions[w_id])

        for line in word_id_file:
            word, self.word_id[word.decode('utf-8')] = line.split()
            word = word.decode('utf-8')

        word_wp_num.close()
        word_id_file.close()

    def get_result(self, query):
        query = (query.decode("utf-8")).lower()
      #  logging.debug("Result for query:" + query)
        return self.Search(query)



def run():

    logging.debug("********************")
    logging.debug("* Input -1 to exit *")
    logging.debug("********************")

    s = Searcher()

    query = '0'
    while query != '-1':
        query = raw_input()
        query = query.lower()
        logging.debug(s.get_result(query))


if __name__ == "__main__":
    s = Searcher()
    #logging.debug(s.get_result("Basel's English Dentist Dr Garry Bonsall"))
    #
    logging.debug(s.get_result("anglaise"))
    #for i in s.get_result("CH"):
    #    logging.debug(i)
    logging.debug(s.get_result("Bahnhofstrasse"))
    logging.debug(s.get_result("Friedrichstrasse"))
    #run()
