import os

def GetMaps(file_index, word_positions, counts):
    
    title_wp = os.path.join('doc', 'WP_' + str(file_index) + '.txt')
    print title_wp
    word_wp_num = open(title_wp, "r+")
    for line in word_wp_num:
        word_id, word_positions[int(word_id)]  = line.split()
        word_id = int(word_id)
        word_positions[word_id] = int(word_positions[word_id])
    word_wp_num.close()

def Merge(file_index_1, word_positions_1, counts_1, file_index_2, word_positions_2, counts_2):

    title_ii_1 =  os.path.join('doc','InvertedIndex_' + str(file_index_1) + '.txt')
    invert_index_1 = open(title_ii_1, "r")
    title_ii_2 =  os.path.join('doc','InvertedIndex_' + str(file_index_2) + '.txt')
    invert_index_2 = open(title_ii_2, "r")
    title_temp =  os.path.join('doc','InvertedIndex_merge.txt')
    if os.path.exists(title_temp):
        os.remove(title_temp)
    invert_index_merge = open(title_temp, "a")
    title_tempWP =  os.path.join('doc','WP_merge.txt')
    if os.path.exists(title_tempWP):
        os.remove(title_tempWP)
    w_p_merge = open(title_tempWP, "a")

    iterator_1 = 0
    iterator_2 = 0
    list_word_id_1 = (word_positions_1.keys())
    list_word_id_2 = (word_positions_2.keys())
    list_word_id_1.sort()
    list_word_id_2.sort()
    while iterator_1 < len(list_word_id_1) and iterator_2 < len(list_word_id_2):
        word_id_1 = list_word_id_1[iterator_1]
        word_id_2 = list_word_id_2[iterator_2]
        if word_id_1 == word_id_2:
            w_p = invert_index_merge.tell()
            invert_index_1.seek(int(word_positions_1[word_id_1]))
            st = ''
            for index in invert_index_1.readline().split():
                st = st + str(index) + " "
            invert_index_2.seek(int(word_positions_2[word_id_2]))

            for index in invert_index_2.readline().split():
                st = st + str(index) + " "
            st += '\n'
       
            invert_index_merge.write(st)
            w_p_num = str(word_id_1) + ' ' + str(w_p) + '\n'
            w_p_merge.write(w_p_num)
            iterator_1 += 1
            iterator_2 += 1
        elif word_id_1 < word_id_2:
            w_p = invert_index_merge.tell()
            invert_index_1.seek(int(word_positions_1[word_id_1]))
            st = ''       
            for index in invert_index_1.readline().split():
               st = st + str(index) + ' '
            st += '\n'
            invert_index_merge.write(st)
            w_p_num = str(word_id_1) + ' ' + str(w_p) + '\n'
            w_p_merge.write(w_p_num)
            iterator_1 += 1
        else:
            w_p = invert_index_merge.tell()
            invert_index_2.seek(int(word_positions_2[word_id_2]))
            st = ''     
            for index in invert_index_2.readline().split():
               st = st + str(index) + ' '
            st += '\n'
            invert_index_merge.write(st)
            w_p_num = str(word_id_2) + ' ' + str(w_p) + '\n'
            w_p_merge.write(w_p_num)
            iterator_2 += 1
    print "in  first"
    while iterator_1 < len(list_word_id_1):
        word_id = list_word_id_1[iterator_1]
        invert_index_1.seek(int(word_positions_1[word_id]))
        st = ''     
        for index in invert_index_1.readline().split():
            st = st + str(index) + ' '
        st += '\n'
        w_p = invert_index_merge.tell()
        invert_index_merge.write(st)
        w_p_num = str(word_id) + ' ' + str(w_p) + '\n'
        w_p_merge.write(w_p_num)
        iterator_1 += 1
    print "in  second"
    while iterator_2 < len(list_word_id_2):
        word_id = list_word_id_2[iterator_2]
        invert_index_2.seek(int(word_positions_2[word_id]))
        st = ''
        for index in invert_index_2.readline().split():
            st = st + str(index) + ' '
        st += '\n'
        w_p = invert_index_merge.tell()
        invert_index_merge.write(st)
        w_p_num = str(word_id) + ' ' + str(w_p) + '\n'
        w_p_merge.write(w_p_num)
        iterator_2 += 1


    invert_index_1.close()
    invert_index_2.close()
    invert_index_merge.close()
    w_p_merge.close()
    
    os.remove(title_ii_1)
    os.remove(title_ii_2)

    #Remove old

    title_old_WP_1 =  os.path.join('doc','WP_' + str(file_index_1) + '.txt')
    title_old_WP_2 =  os.path.join('doc','WP_' + str(file_index_2) + '.txt')
    print file_index_1
    print file_index_2

    os.remove(title_old_WP_1)
    os.remove(title_old_WP_2)

    title_new =  os.path.join('doc','InvertedIndex_' + str(file_index_1 / 2) + '.txt')
    title_new_WP =  os.path.join('doc','WP_' + str(file_index_1 / 2) + '.txt')


    os.rename(title_temp, title_new)
    os.rename(title_tempWP, title_new_WP)
    print "rename" + str(file_index_1 / 2)
    
def MergeFiles(NUMBER_OF_FILES):
    iterator = 0
    if NUMBER_OF_FILES <= 1:
        return
    while iterator + 1 < NUMBER_OF_FILES:
        print iterator
        counts_1 = {}
        word_positions_1 = {}
        GetMaps(iterator, word_positions_1, counts_1)
        print "******"
        word_positions_2 = {}
        counts_2 = {}
        GetMaps(iterator + 1, word_positions_2, counts_2)
        Merge(iterator, word_positions_1, counts_1, iterator + 1, word_positions_2, counts_2)
        iterator += 2

    MergeFiles(NUMBER_OF_FILES / 2)
    if NUMBER_OF_FILES % 2 == 1:
        counts_1 = {}
        word_positions_1 = {}
        GetMaps(0, word_positions_1, counts_1)
        word_positions_2 = {}
        counts_2 = {}
        GetMaps(NUMBER_OF_FILES - 1, word_positions_2, counts_2)
        Merge(0, word_positions_1, counts_1, NUMBER_OF_FILES - 1, word_positions_2, counts_2)


listdir = os.listdir("doc")
mergelist = set()
for fname in listdir:
    if fname.startswith("InvertedIndex"):
        mergelist.add(fname)
print len(mergelist)
MergeFiles(len(mergelist))



