# -*- coding: CP1251 -*-
import os
import re
import math

invertedIndex = {}
word_id = {}
node_info = {}

WORD_RE = re.compile(u"<tag k=* v=*/>")
WORD_FOR_LOOKING = [u"addr:city", u"addr:country", u"addr:street"]


def AddWords(line, page_id, number_word_id, key, is_it_way):
    word_in_doc = set()

    line_list = re.split("-|;|'|,| |/|\r|\t|&", line)
    line_list = " ".join(line_list).split()

    priority = len(line_list)
    node_info[(page_id, key)] = map(lambda x: str(x), [priority, is_it_way])

    for word in line_list:
        word = word.lower()
        if word_id.has_key(word):
            number = word_id[word]
            word_in_doc.add(number)
            if invertedIndex.has_key(number):
                if page_id not in invertedIndex[number]:
                    invertedIndex[number].add((page_id, key))
            else:
                invertedIndex[number] = set()
                invertedIndex[number].add((page_id, key))
        else:
            word_id[word] = number_word_id
            word_in_doc.add(number_word_id)
            invertedIndex[number_word_id] = set()
            invertedIndex[number_word_id].add((page_id, key))
            number_word_id += 1
    return number_word_id

def WriteFiles(title_inv, title_wp):
    invert_index = open(title_inv, "w+")
    word_wp_num = open(title_wp, "w+")
    for key in invertedIndex.keys():
        string = ''
        for node_id, addr in invertedIndex[key]:
            string = string + str(node_id) + ' ' + addr + ' ' + " ".join(node_info[(node_id, addr)]) + ' '
        number = invert_index.tell()     
        string1 =  str(key) + ' ' + str(number) + '\n'
        string += '\n'
        invert_index.write(string)
        word_wp_num.write(string1)
    invertedIndex.clear()
    word_wp_num.close()
    invert_index.close()
           

print("*********")


if not os.path.exists('doc'):
    os.makedirs('doc')


file_number = 0
number_of_pages = 0
number_of_doc = 0
number_word_id = 0
mapify_data = open("../data/switzerland-latest.osm", "r")
#mapify_data = open("../data/test.txt", "r")
MAX_PAGES_COUNT = 100000

WORD_RE = re.compile(u"city")

key_name = "NONAME"
           
STOP_WORDS = ['ele', 'name', 'operator', 'public_transport', 'uic_name', 'uic_ref', 'url'] 
#looking_for_name = False
stop_looking_node_info = True
stop_looking_way_info = True


def parse_node_info(node_id, begin_page_id, end_page_id, stop_looking_node_info):
    if(begin_page_id != -1) and line[-3:] != "/>\n":
        # data format: <node id="*" lat=* lon=* version=* timestamp=* changeset=* uid=* user=*/>
        node_id = line.split()[1][len("id=\""):-1]
        stop_looking_node_info = False

    #if line[-2:] == '/>':
    #    stop_looking_node_info = True

    if(end_page_id != -1):
        key_name = "NONAME"
        stop_looking_node_info = True
    return (stop_looking_node_info, node_id)

node_id = -1
way_id = -1

for line in mapify_data:
    # begin_page_id = line.find('<node')
    # end_page_id = line.find('</node>')

    (stop_looking_node_info, node_id) = parse_node_info(node_id, line.find('<node'), line.find('</node>'), stop_looking_node_info)
    (stop_looking_way_info, way_id) = parse_node_info(way_id, line.find('<way'), line.find('</way>'), stop_looking_way_info)


    if not stop_looking_node_info or not stop_looking_way_info:
        if line.find("<tag") != -1:
            key = line.split()[1][len("k=\""):-1].decode('cp1251').encode('utf-8')
            index_value = line.find("v=\"")
            if index_value != -1:
                value = line[index_value+len("v=\""):-len("\"/> ")]
            else:
                value = "NONE"

            if key in WORD_FOR_LOOKING:
                if stop_looking_node_info:
                    number_word_id = AddWords(value, way_id, number_word_id, key, True)
                else:
                    number_word_id = AddWords(value, node_id, number_word_id, key, False)

	        number_of_pages += 1

        if number_of_pages == MAX_PAGES_COUNT:
            title_inv =  os.path.join('doc','InvertedIndex_' + str(file_number) + '.txt')
            title_wp =  os.path.join('doc','WP_' + str(file_number) + '.txt')
            WriteFiles(title_inv, title_wp)
            number_of_pages = 0
            file_number += 1
            print file_number



if number_of_pages != 0:
    title_inv =  os.path.join('doc','InvertedIndex_' + str(file_number) + '.txt')
    title_wp =  os.path.join('doc','WP_' + str(file_number) + '.txt')
    WriteFiles(title_inv, title_wp)
    number_of_pages = 0
    file_number += 1
    print file_number

w_id = open("word_id.txt", "w")
for word in word_id.keys():
    print word
    w_id.write(str(word) + ' ' + str(word_id[word]) + ' ' + '\n')
w_id.close()

   

mapify_data.close()
