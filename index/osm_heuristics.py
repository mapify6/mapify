'''
Created on May 3, 2014

@author: Amit
'''
from collections import *

def analyse_index_output(list_elements):
    exact_matches = [ x for x in list_elements if (x[2] == '1')]
    dict_v = defaultdict(int)
    for (x,y,z,w) in exact_matches:
        dict_v[y] += 1
        
    if "addr:city" in dict_v and dict_v["addr:city"] > 0.1 * len(exact_matches):
        return "city"
    
    if "addr:street" in dict_v and dict_v["addr:street"] > 0.1 * len(exact_matches):
        return "street"
    
    if "addr:country" in dict_v and dict_v["addr:country"] > 0.1 * len(exact_matches):
        return "country"
    
    return ""