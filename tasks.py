from celery import Celery

celery = Celery('tasks',backend="amqp",  broker='amqp://lsir-cluster-01.epfl.ch:5672/')

def format_url(url):
    ans = url
    if not url.startswith("http"):
        ans = "http://" + ans
    return ans  

@celery.task
def download_url(url):
    url = format_url(url)
    print "Retrieving URL-->" + url + "<---"
    r = requests.get(url)
    if r.status_code != 200:
        print "Error in downloading URL" +  str(url)
    else:
        return r.text 
