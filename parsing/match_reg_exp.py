'''
Created on May 16, 2014

@author: Amit
'''
import nltk
import string
from nltk.util import ngrams
import numpy as np
import re
import math
import psycopg2
from collections import defaultdict
import requests


conn = psycopg2.connect("dbname = 'osm' user = 'osm' host = 'lsir-cluster-02.epfl.ch' password = 'osm'")
static_keywords_dict = defaultdict(list)
reg_exp_dict = defaultdict(list)
static_keywords_regex_dict = {}
reg_exp_regex_dict = {}
#holidays_regexp = ""
list_postcodes = []



def wrap_regex(rex):
    return "^" + rex + "$|\s" + rex + "$|^" + rex + "\s|\s" + rex + "\s" 
    

 
def load_regexp():
    cur = conn.cursor()
    
    cur.execute("select * from StaticKeywords where type != 'postcode'")
    for x in cur:
        typev = x[2]
        if typev not in ["roads", "postcode","canton"]:
            static_keywords_dict[typev].append(wrap_regex(x[1]))

    for k,v in static_keywords_dict.items():
        
        static_keywords_dict[k] = sorted(static_keywords_dict[k], key = lambda x : -1 * len(x))   
        static_keywords_regex_dict[k] = re.compile("|".join(static_keywords_dict[k]),re.IGNORECASE)
        
    cur.execute("select * from StaticKeywords where type = 'postcode'")
    for x in cur:
	list_postcodes.append(str(x[1]))
    
    cur.execute("select * from RegularExpressions")
    for x in cur:
        typev = x[2]
        if typev not in ["days","hours_indicator","holiday hours","canton"]:
            reg_exp_dict[typev].append(wrap_regex(x[1]))

    
    for k,v in reg_exp_dict.items():
        #print k,v
        reg_exp_dict[k] = sorted(reg_exp_dict[k], key = lambda x : -1 * len(x))   
	reg_exp_regex_dict[k] = re.compile("|".join(reg_exp_dict[k]),re.IGNORECASE)
            
    
    cur.execute("select * from Holidays")
    holidays_list = []
    for x in cur:
        holidays_list.append(wrap_regex(x[1]))    
    
  #  holidays_regexp = re.compile("|".join(holidays_list))
    

def apply_regexp(string,list_regexp):
    ans = defaultdict(lambda: None)
    len_dict = defaultdict(list)
    for i,rg in enumerate(list_regexp):
        matches = re.finditer(rg,string)
        for k in matches:
            st = k.start()
            end = k.end()
            mat_str = string[st:end]
            word_start = len([x for x in string[0:st] if x ==" "]) + 1
            if st == 0:
                word_start = 0 
            word_end = len([x for x in string[0:end] if x ==" "]) + 1
            #print word_start,word_end,mat_str, rg.pattern
            
            for j in range(word_start,word_end):
                len_dict[j].append((word_end - word_start,i,word_start,mat_str))
            ans[word_start]=i
    
    new_ans_dict = defaultdict(lambda: None)
    for k,v in len_dict.items():
        s_v = sorted(v)
        new_ans_dict[s_v[-1][2]] = (s_v[-1][1],s_v[-1][3])
    
    return new_ans_dict


def find_matches(string):
    tokens = string.split()
    stk_out = apply_regexp(string, static_keywords_regex_dict.values())
    regex_out = apply_regexp(string,reg_exp_regex_dict.values())
    stk_keys = static_keywords_regex_dict.keys()
    regex_keys = reg_exp_regex_dict.keys()
    
    ans = []
    
    for k,v in stk_out.items():
        typev = stk_keys[v[0]]
        len_t = len(v[1].split())
        ans.append((typev,k,len_t))
        
    for k,v in regex_out.items():
        
        typev = regex_keys[v[0]]
        len_t = len(v[1].split())
        ans.append((typev,k,len_t))    
        
    
    for i,t in enumerate(tokens):
	if t in list_postcodes:
	     ans.append(('postcode',i,1))     
 
    return ans    




load_regexp()

if __name__ == '__main__':
    
    #print static_keywords_regex_dict
    #print reg_exp_regex_dict
    #print reg_exp_regex_dict['house_number']
    url = "http://en.yelp.ch/search?find_desc=Haircut&find_loc=Lausanne%2C+Vaud"
    r = requests.get(url)
    text = r.text
    clean_text = nltk.clean_html(text).encode('utf-8')
    clean_text = clean_text.replace("\n", " _ ")
    clean_text = " ".join(clean_text.strip().split())
    print clean_text
    matches = find_matches(clean_text)
    print matches
    tokens = clean_text.split()
    for x,y,z in matches:
        print x,tokens[y:y+z],y
