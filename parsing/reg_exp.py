# -*- coding: utf-8 -*-
import nltk
import string
import re
from nltk.util import ngrams
import numpy as np
import re
import math
import psycopg2

def reg_exp_bigdata(myList):
    zipcode = [];
    streetnumber = [];
    streetname = [];
    housenumber = [];
    street_name_ind = [];
    
    def qualify_address(w):
        address = 0;
        if((re.match(route_regexp,w,re.IGNORECASE)  is not None) or (re.search(zipcode_search,w,re.IGNORECASE) is not None)): 
            address = 1;
        
        return address
        
    def qualify_days_hours(w):
        hours_days = 0;
        #if(re.search('^[lundi | mardi | mercredi | jeudi | vendredi | samedi | dimanche | monday | tuesday | wednesday | thursday | friday | saturday | sunday]$',w,re.IGNORECASE)  is not None or re.search('| mo | dites | murs | toit | fr | cette | aller | horaires | d ouverture | ferme | regulier | habituel | ouvert',w,re.IGNORECASE)is not None):
        if(re.search(days_hours[0],w,re.IGNORECASE)  is not None or re.search(days_hours[1],w,re.IGNORECASE)is not None):
            hours_days = 1;
        return hours_days
        
    def qualify_holidays(w):
        holi = 0;
        if (re.search(holidays_hours[0], w,re.IGNORECASE) or re.match(holidays_hours[1], w,re.IGNORECASE) or re.search(holidays_hours[2], w, re.IGNORECASE)):
            holi = 1;
        return holi
        
        
    def street_name_number_finder(w):
        z = len(w);
        #print "length is:" + str(z)
        street_name = [];
        building_number = [];
        for word in w:
            x = word.lower()
            yy = [];
            for c in List_route:
                yy.append(x.find(c));
            #y =re.search('strasse$',x)
            #print y
            if((x in List_route and x is not '') or any(xy>=0 for xy in yy)):
                street_name.append(word)
        
        
        if(street_name != []):
            y_end = w.index(street_name[-1]);
            y_start = w.index(street_name[0]);
            
            # if the street name is between the address
            if(y_start is not 0 and y_end is not z-1):
                if(re.match(building_numbers,w[y_start-1],re.IGNORECASE) is not None and re.match(zipcode_search,w[y_start-1]) is None ):
                    building_number = w[y_start-1];
                    #w.remove(building_number);
                elif(re.match(building_numbers,w[y_end+1],re.IGNORECASE) is not None and re.match(zipcode_search,w[y_end+1]) is None ):
                    building_number = w[y_end+1];
                    #w.remove(building_number);
            # if the street name is at the start of address
            elif(y_start is 0 and y_end is not z-1):
                if(re.match(building_numbers,w[y_end+1],re.IGNORECASE) is not None and re.match(zipcode_search,w[y_end+1]) is None ):
                    building_number = w[y_end+1];
                    #w.remove(building_number);
            # if the street name is at the end of address
            elif(y_start is not 0 and y_end is z-1):
                if(re.match(building_numbers,w[y_start-1],re.IGNORECASE) is not None and re.match(zipcode_search,w[y_start-1]) is None ):
                    building_number = w[y_start-1];
                    #w.remove(building_number);
        #print building_number   
        y_building_number = [];         
        if(building_number == []):
            #print "I am here"
            for word in w:
                if(re.match(building_numbers,word.lower(),re.IGNORECASE) is not None and re.match(zipcode_search,word.lower()) is None and re.match('[a-z]+',word.lower()) is None ):
                    #print word
                    building_number = word
                    y_building_number = w.index(building_number);
                    #print building_number
                    #print y_building_number
                    break;
            
        # check if the street name is full or not
        if(street_name != [] and y_building_number != [] and y_building_number - y_end >0):
            street_name = street_name + w[y_end+1:y_building_number];
        
        
        return [street_name, building_number, w]
        
    
        
    def city_finder(w):
        city = [];
        return city
        
    def zipcode_finder(w):
        #print "hello"
        #print w
        zipcode = [];
        for word in w:
            if(re.match(zipcode_match,word,re.IGNORECASE) is not None or re.search(zipcode_search,word,re.IGNORECASE) is not None):
                zipcode = word
        #print zipcode
        #if(zipcode):
        #    w.remove(zipcode) 
        #print "w after zipcode removal is:", w       
        return [zipcode, w]
    
    def canton_finder(w):
        canton = [];
        for word in w:
            x = word.lower()
            yy = [];
            for c in List_canton:
                if(x == c.lower()):
                    yy.append(1);
            if(any(xy >=0 for xy in yy)):
                canton.append(word);
                
        #print canton
        #if(canton):
        #    w.remove(canton)
        return [canton, w]
        
    def hours_days_finder(w):
        print len(w)
        #print w
        j = 0;
        days = [];
        open_hours = [];
        close_hours = [];
        for i in range(0,len(w)):
            
            x = w[i].lower()
            if(x in List_days and (w[i-1].lower() != 'bis' or w[i-1].lower() != 'to' or w[i-1].lower() != '-')):
                # day of the week has been mentioned here, go to find the open and close hours
                days.append(w[i]);
                #print hours_days
                #check if the open and close hours are mentioned next to it:
            elif(re.match(normal_hours[0],x) is not None or re.match(normal_hours[1],x) is not None ):
                if(w[i-1] == '-' or w[i-1].lower() == 'to' or w[i-1].lower() == 'a' or w[i-1].lower() == 'bis'):
                    # open hours have been mentioned here, if this condition is true
                    close_hours.append(x)
                elif(i+1<=len(w)-1):
                    if( w[i+1] == '-' or w[i+1].lower() == 'to' or w[i+1].lower() == 'a' or w[i+1].lower() == 'bis'):
                        open_hours.append(x);
                    elif(i+2<=len(w)-1):
                        if(w[i+2].lower() == '-' or w[i+2].lower() == 'to' or w[i+2].lower() == 'a' or w[i+1].lower() == 'bis'):
                            # open hours have been mentioned here, if this condition is true
                            open_hours.append(x)
                        else:
                            open_hours.append(x)
            elif(x in List_days and (w[i-1].lower() == 'bis' or w[i-1].lower() == 'to' or w[i-1].lower() == '-')):
                days[-1] = days[-1]+' '+w[i]
            elif((x == 'bis' or x == 'to' or x == '-') and (w[i-1].lower() in List_days)):
                days[-1] = days[-1]+' '+w[i]
            elif(x == 'closed' or x =='ferme'):
                open_hours.append('-')
                close_hours.append('-')
            # before moving to closing hours check if there is 'am' or 'pm' written after opening hours
                
        return [days, open_hours, close_hours]
        
        
    
    
    def holiday_finder(w):
        holidays = [];
        holidays_open_time = []
        holidays_close_time = []
        berry_idx = [i for i, item in enumerate(w) if (re.match(holidays_hours[0], item) or re.match(holidays_hours[1], item or re.match(holidays_hours[2], item)))]
        #print berry_idx
        
        for i in range(0,len(berry_idx)):
            if(i+1 <=len(berry_idx)-1):
                h = berry_idx[i+1];
            else:
                h = len(w);
            ws = w[berry_idx[i]:h];
            holidays.append(w[berry_idx[i]]);
            #print ws
            #print w[berry_idx[i+1]]
            for i in range(0,len(ws)):
                    if(re.match(normal_hours[0],ws[i]) is not None or re.match(normal_hours[1],ws[i]) is not None ):
                        if(ws[i-1] == '-' or ws[i-1].lower() == 'to' or ws[i-1].lower() == 'a' or ws[i-1].lower() == 'bis'):
                            # open hours have been mentioned here, if this condition is true
                            holidays_close_time.append(ws[i])
                        elif(i+1<=len(ws)-1):
                            if( ws[i+1] == '-' or ws[i+1].lower() == 'to' or ws[i+1].lower() == 'a' or ws[i+1].lower() == 'bis'):
                                holidays_open_time.append(ws[i]);
                            elif(i+2<=len(ws)-1):
                                if(ws[i+2].lower() == '-' or ws[i+2].lower() == 'to' or ws[i+2].lower() == 'a' or ws[i+1].lower() == 'bis'):
                                    # open hours have been mentioned here, if this condition is tru
                                    holidays_open_time.append(ws[i])
                                else:
                                    holidays_open_time.append(ws[i])
                    elif(ws[i].lower() == 'closed' or ws[i].lower() =='ferme'):
                        holidays_open_time.append('-')
                        holidays_close_time.append('-')
                        
    
        return [holidays, holidays_open_time, holidays_close_time];
    
    # get all the text
    #zipcode_search = '[1][0][0-9][0-9]';
    #
    #zipcode_match = '[CH-]*[1][0][0-9][0-9]';
    #
    #building_numbers = '[0-9]+[a-z]*';
    #
    #normal_hours = ['[0-9]*:[0-9][0-9]','[0-9]*h[0-9][0-9]'];
    #
    #holidays_hours = ['[0-9]+.[0-9]+.[0-9]+', '[0-9]+:[0-9]+:[0-9]+', '[0-9]+-[0-9]+-[0-9]+']
    #
    ##holidays_date = ['Nouvel An','Vendredi Saint', 'Lundi de Paques', 'Jeudi de l Ascension', 'Lundi de Pentecote', 'Fete nationale suisse', 'Lundi du Jeune', 'Noel','Saint Etienne','Restauration de la Republique Geneve','Fete-Dieu','Fete de l Assomption', 'Saint Maurice', 'Toussaint', 'Immaculee Conception', 'La fete du Travail', 'Immaculee Conception', 'Nouvel An','Sts Pierre et Paul','Saint James','Dimanche de St Martin', 'Lundi de St Martin', 'Jour de Saint-Andre','Saint Conrad', 'Saint Gall','Saint-Gall', 'Saint Leger','Sainte-Therese','Jour Saint Jerome', 'Saint-Michel','Saint Justin','Saint Ulrich','Saint John','Eglise anniversaire','Eglise vacances','Saint Sigismond','Engelweihe', 'Jeune federal', 'Marys la naissance'];
    #route_regexp = '[bahnhof|gare|srain|garten|gasse|platz|markt| quai|cours| weg |place|passage | Rue | rue | rue de la | rue du | rue de |chemin des | chemin du | grand rue | avenue de | route | ch. de | rte | strasse | route de| rue des | avenue des]';
    #
    #List_days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche', 'monday', 'tuesday', 'wednesday', 'thursday' , 'friday' , 'saturday' , 'sunday', 'mo', 'sa','so','do','dites','murs','toit','fr','cette','aller']
    ## from indexed data
    #List_canton = ['zh', 'be', 'lu', 'ur', 'sz', 'ow', 'nw', 'gl', 'zg', 'fr', 'so', 'bs', 'bl', 'sh', 'ar', 'ai', 'sg', 'gr', 'ag', 'tg', 'ti', 'vd', 'vs', 'ne', 'ge', 'ju','zurich', 'bern', 'berne', 'luzern', 'uri', 'schwyz', 'unterwalden', 'obwalden', 'nidwalden', 'glarus', 'zug', 'freiburg', 'fribourg', 'solothurn', 'basel', 'basel stadt', 'stadt', 'land', 'basel land', 'schaffhausen','appenzell', 'appenzell ausserrhoden', 'ausserhoden','appenzell innerrhoden', 'innerhoden','st.', 'gallen','st. gallen', 'sankt gallen', 'graubunden', 'aargau', 'thurgau', 'ticino', 'vaud', 'valais', 'wallis', 'neuchatel', 'geneve', 'jura'];
    #
    ##List_route = ['bahnhof','gare','srain','garten','gasse','platz','markt','quai','cours','weg','place','passage','rue','des','chemin', 'du', 'grand', 'avenue', 'route', 'ch.' ,'de'  ,'rte','strasse'];
    #
    #List_route = ['bahnhof','gare','srain','garten','gasse','platz','markt','quai','cours','weg','place','passage','rue','des','chemin', 'grand', 'avenue', 'route'  ,'rte','strasse','route de' , 'avenue de','av','av du','port','Allee','rue des','avenue des'];
    #
    #
    #days_hours = ['^[lundi | mardi | mercredi | jeudi | vendredi | samedi | dimanche | monday | tuesday | wednesday | thursday | friday | saturday | sunday]$','| mo | dites | murs | toit | fr | cette | aller | horaires | d ouverture | ferme | regulier | habituel | ouvert'];
    #
    #from the static keywords table
    #List_route = ['bahnhof','gare','srain','garten','gasse','platz','markt','quai','cours','weg','place','passage','rue','des','chemin', 'du', 'grand', 'avenue', 'route', 'ch.' ,'de'  ,'rte','strasse'];
    
    ##build regular expressions from the obtained information             
    
    
    
    ##################################
    ##################################
    #The main code starts here
    #filepath_name = '/Users/naumanshahid/Dropbox/python/address2.txt';
    #fp_myList = open(filepath_name,'r')
    #myList =fp_myList.read();
    myList = myList.split(' ')
    
    
    
    words = " ";        
    for i in range(0,len(myList)):
        words = str(words) + myList[i] + " ";
    
    conn = psycopg2.connect("dbname = 'osm' user = 'osm' host = 'localhost' password = 'osm'")
    
    cur = conn.cursor()
    xyz = [];
    
    holidays_date = [];
    cur.execute("SELECT name FROM Holidays")
    for record in cur:
        holidays_date.append(record[1])
        
    List_route = [];
    cur.execute("SELECT keyword FROM StaticKeywords where type = 'roads'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        List_route.append(record[0])
        
    List_canton = [];    
    cur.execute("SELECT keyword FROM StaticKeywords where type = 'canton'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        List_canton.append(record[0])
    
    zipcode_search = [];
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'zipcode search' and language = 'french'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        zipcode_search = record[0]
        
    zipcode_match = [];    
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'zipcode match' and language = 'french'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        zipcode_match = record[0]
     
    building_numbers = [];       
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'house_number' and language = 'french'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        building_numbers.append(record[0])
    
    normal_hours = [];        
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'hours' and language = 'french'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        normal_hours.append(record[0])
    
    holidays_hours = [];
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'holiday hours' and language = 'french'")
    for record in cur:
        #xyz = cur.fetchone()[0]
        holidays_hours.append(record[0])
    
    route_regexp = [];
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'route' and language = 'french'")
    for record in cur:
        route_regexp.append(record[0])
    
    days_hours = [];
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'days' and language = 'french'")
    for record in cur:
        days_hours.append(record[0])
    
    
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'days' and language = 'german'")
    for record in cur:
        days_hours.append(record[0])
    
    
    cur.execute("SELECT reg_ex FROM RegularExpressions WHERE type = 'hours_indicator' and language = 'french'")
    for record in cur:
        days_hours[1] = days_hours[1] + record[0]
    
    List_days = [];
    cur.execute("select * from weekdays_ml where lower(language) in('french','english','german');")
    for record in cur:
        #xyz = cur.fetchone()[0]
        List_days.append(record[2])
        
    
    #print words                                    
    address = qualify_address(words);  # if some portion qualifies as address the proceed forward to identfy address portions
    hours_days = qualify_days_hours(words);
    holidays_mention = qualify_holidays(words);
    
    output = {}
        
    if(address == 1):
        words1 = [];
        
        words1 = words;
        
        words1 = nltk.word_tokenize(words1)
        words1 = [word.strip(',\n\t$%&*(){}[]#@!.') for word in words1];  ## strip the useless portions out of address
        words2 = words1
        while '' in words1:
            words1.remove('')
        
        zipcode, words1 = zipcode_finder(words1);             # find the zipcode
        if(zipcode != []):
            if(words1.index(zipcode)-10<0):
                index_start = 0
            else:
                index_start = words1.index(zipcode)-10
            if(words1.index(zipcode)+5 > len(words1)-1):
                index_end = len(words1)-1
            else:
                index_end = words1.index(zipcode)+5
                
            words1 = words1[index_start:index_end]
        #print words1
        street_name, building_number, words1 = street_name_number_finder(words1); 
        
        canton,words1 = canton_finder(words1);                # find the canton
        if(canton != []):
            canton = ''.join(canton)
        #print canton
        #print words1
        city = []
        
        if(canton!=[] and canton in words1):
            city = words1[words1.index(canton)-1]
        elif(zipcode!=[] and zipcode in words1):
            city = words1[words1.index(zipcode)+1]
        
        print "street name is:", street_name
        print "building number is:", building_number
        print "zipcode is:", zipcode
        print "City is:" , city
        print "canton is:", canton
        
        MyList = words2
        
        street_name_index = [];
        if(street_name != []):
            if(len(street_name) > 1):
                for i in range(0,len(street_name)):
                    if(street_name[i] in MyList):
                        street_name_index.append(MyList.index(street_name[i]))
            elif(street_name[0] in MyList):
                street_name_index = str(MyList.index(street_name[0]))
                
        output['street'] = street_name_index
        
        building_number_index = [];
        if(building_number != []):
            if(building_number in MyList):
                building_number_index = (MyList.index(building_number))
    
        
        output['housenumber'] = building_number_index
        
        zipcode_index = [];
        if(zipcode != [] and zipcode in MyList):
            zipcode_index = (MyList.index(zipcode))
        output['zipcode'] = zipcode_index
        
        city_index = [];
        if(city != [] and city in MyList):
            city_index = (MyList.index(city))
        output['city'] = city_index
        
        canton_index = [];
        if(canton != [] and canton in MyList):
            #if(len(canton)>1):
            #    canton_index = str(MyList.index(canton[0]))+'-'+str(MyList.index(canton[-1]))
            #else:
            canton_index = (MyList.index(canton))
        output['canton'] = canton_index
        
        
    
    words = myList
    words = ' '.join(words)
    
    #print hours_days
    if(hours_days == 1):
        
        words = [word.strip(',\n\t$%&*(){}[]#@!:;.') for word in nltk.word_tokenize(words)];
        while '' in words:
            words.remove('')
    
        days, open_hours, close_hours = hours_days_finder(words)
        print days
        print open_hours
        print close_hours
        
        days_index = [];
        if(days != []):
            days = ' '.join(days)
            days = days.split(' ')
            for i in range(0,len(days)):
                if(days[i].isalpha and days[i] in MyList):
                    days_index.append((MyList.index(days[i])))
        
        output['days'] = days_index
        
        open_hours_index = [];
        if(open_hours != []):
            for i in range(0,len(open_hours)):
                if(open_hours[i] in MyList):
                    open_hours_index.append(MyList.index(open_hours[i]))
                
        output['open_hours'] = open_hours_index
        
        close_hours_index = [];
        if(close_hours != []):
            for i in range(0,len(close_hours)):
                if(close_hours[i] in MyList):
                    close_hours_index.append(MyList.index(close_hours[i]))
        output['close_hours'] = close_hours_index
        
        
    if(holidays_mention == 1): 
        holidays, holidays_open_time, holidays_close_time = holiday_finder(words)
        print holidays
        print holidays_open_time
        print holidays_close_time
        
        holidays_index = [];
        if(holidays != []):
            for i in range(0,len(holidays)):
                holidays_index.append(MyList.index(holidays[i]))
            
        output['holidays'] = holidays_index
        
        holidays_open_time_index = [];
        if(holidays_open_time != []):
            for i in range(0,len(holidays_open_time)):
                if(holidays_open_time[i] in MyList):
                    holidays_open_time_index.append(MyList.index(holidays_open_time[i]))
        output['holidays_open_time'] = holidays_open_time_index
        
        holidays_close_time_index = [];
        if(holidays_close_time != []):
            for i in range(0,len(holidays_close_time)):
                if(holidays_close_time[i] in MyList):
                    holidays_close_time_index.append(MyList.index(holidays_close_time[i]))
        
        output['holidays_close_time'] = holidays_close_time_index
        
        
        
        
    #return [street_name, building_number, zipcode, city, canton, days, open_hours, close_hours, holidays, holidays_open_time, holidays_close_time]
    return output