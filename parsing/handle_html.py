'''
Created on May 3, 2014

@author: Amit
'''
import nltk
import copy
from nltk.corpus import stopwords
from index.osm_heuristics import *
from index.osm_heuristics import analyse_index_output
from parsing.reg_exp import reg_exp_bigdata
from numpy import median
from imposm.geocoder.model import init_model, geocode
from imposm.geocoder.config import load_config
from scripts.geocodeconverter import MetersToLatLon
from nltk.metrics.distance import edit_distance
from parsing.match_reg_exp import find_matches
from functools import wraps
import math

index_searcher_holder = None

conf = ""
punctuations = ["/","(",")","\\","|", ":",",",";",".","?", "!"]

stop = stopwords.words('english')
geocoder_file = "/home/mapify/mapify/scripts/geocoder.ini"

default_json = """{
  "Data": [ {
      "Place" : {
          "name" : "EPFL",
          "lat" : 46.5203,
          "lon" : 6.5656,
          "description" : "hell",
          "rating" : 3
      },
      "OpeningHours" : [ {
          "open_hour" : 600,
          "close_hour" : 1200
          },{
          "open_hour" : 1300,
          "close_hour" : 2000
          }
          ]
     
  }, {
      "Place" : {
          "name" : "UNIL",
          "lat" : 46.523,
          "lon" : 6.586,
          "description" : "heaven22",
          "rating" : 5
      },
      "OpeningHours" : [ {
          "open_hour" : 900,
          "close_hour" : 1000
          },{
          "open_hour" : 1200,
          "close_hour" : 1700
          }
          ]
     
  }
]

}"""

default_dict = {
"Place" : {
  "name" : "EPFL", 
  "lat" : 46.5203, 
  "lon" : 6.5656, 
  "description" : "Hell", 
  "rating" : 3},
"OpeningHours" : [{
  "open_hour" : 600,
  "close_hour" : 1200
  },{
  "open_hour" : 1300,
  "close_hour" : 2000
  }]
}

default_results = [{
"Place" : {
  "name" : "EPFL", 
  "lat" : 46.5203, 
  "lon" : 6.5656, 
  "description" : "Hell", 
  "rating" : 3},
"OpeningHours" : [{
  "open_hour" : 600,
  "close_hour" : 1200
  },{
  "open_hour" : 1300,
  "close_hour" : 2000
  }]
},
{
"Place" : {
  "name" : "UNIL", 
  "lat" : 46.52256, 
  "lon" : 6.57927, 
  "description" : "Heaven", 
  "rating" : 3},
"OpeningHours" : [{
  "open_hour" : 600,
  "close_hour" : 1200
  },{
  "open_hour" : 1300,
  "close_hour" : 2000
  }]
},
{
"Place" : {
  "name" : "HEC", 
  "lat" : 46.52240, 
  "lon" : 6.58446, 
  "description" : "Here rules the $$$", 
  "rating" : 3},
"OpeningHours" : [{
  "open_hour" : 600,
  "close_hour" : 1200
  },{
  "open_hour" : 1300,
  "close_hour" : 2000
  }]
}]


unicode_file = "data/unicode.csv"
unicode_map = {}
def load_unicode():
    lines = []
    with open(unicode_file) as fp: 
	lines = fp.readlines()
    
    for line in lines:
        tokens = line.split(",")
	st = int("0x" + tokens[0].lstrip('0'),16)
        end = int("0x" + tokens[1].lstrip('0'),16)
        for ch in range(st,end+1):
	    unicode_map[ch] = tokens[2].strip()


load_unicode()


def convert_string_to_unicode(string):
    ans = ""
 #   print unicode_map	
    for ch in string.decode('utf-8'):
 #       print ch
        chn = ord(ch)
 #       print chn
#	    print "added -->xx" + chr(chn) + "xx"
        if chn in unicode_map:
	    ans = ans + unicode_map[chn]
#            print "added -->xx" + unicode_map[chn]  + "xx"
        elif chn < 256:
	    ans = ans + chr(chn)
        else:
            ans = ans + "a"   			
    return ans


 
def is_word(token):
    return (any(c.isalpha() for c in token))

#TODO - write code to split a html into different divs, then get text for each div and get output 
def handle_html(html,index_searcher):
    raw_text = nltk.clean_html(html)
    raw_text = raw_text.replace("\n"," _ ")
    for x in punctuations:
        raw_text = raw_text.replace(x, " " + x + " ")  
    raw_text = " ".join(raw_text.split())
    print "Raw Text----------------"
    print(raw_text)
    print "------------------------"
    info = get_information(raw_text,index_searcher)
    return info


def memo(func):
    cache = {}
    @wraps(func)
    def wrap(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrap


@memo
def search_using_index(token):
    return index_searcher_holder.get_result(token)

#TODO - write mapping for unicode conversion like Zrich
def tag_using_indices(tokens):
    tags = []
    for i,token in enumerate(tokens):
        index_output = ""
        if token not in stop and is_word(token):
            try:
                index_output = search_using_index(token)
            except:
                pass
        
        if index_output:
            typev = analyse_index_output(index_output)
            if typev and typev != "country":
                print i,typev, tokens[i]
                tags.append((typev,i))
        
    return tags

#def tag_using_regular_exps(text):
            

# def combine_tags(tokens,index_tags,reg_ex_tags):
#     token_id_type_dict = {}
#     for (x,y) in index_tags:
#         token_id_type_dict[y] = (x,"i")
#     
#     for k,v in reg_ex_tags.items():
#         if k in ["city"]:
#             continue
#         
#         if isinstance(v,list):
#             for x in v:
#                 token_id_type_dict[int(x)] = k
#         elif v:
#             token_id_type_dict[int(v)] = (k,"r")
#     return token_id_type_dict                    


def map_key_name(k):
    ans = k
    if k =="street" or k =="route" or k == "roads" or k == "road":
	ans = "road"
    elif k == "zipcode" or k == "postcode":
        ans = "postcode"
    elif k == "house_number":
        ans = "housenumber"
    elif k == "phone_number1":
        ans = "phone_number"
    return ans




def combine_tags(tokens,index_tags,reg_ex_tags):
    token_id_type_dict = {}
    for (x,y) in index_tags:
        token_id_type_dict[y] = (map_key_name(x),"i",1)
    
    for typev,start,lenv in reg_ex_tags:
        typev = map_key_name(typev)
        if typev in ["city"]:
            continue
        
        token_id_type_dict[start] = (typev,"r",lenv)
        
    return token_id_type_dict      



def load_geocoder():
    conf = load_config(geocoder_file)
    init_model(conf)

load_geocoder()

def valid_config(dictv,listk):
    for k in listk:
        if k not in dictv:
            return False
    return True    
            


    
def map_dict(dictv):
    ans = {}
    for k,v in dictv.items():
        ans[map_key_name(k)] = v
    for k in ['city','road','country','postcode','housenumber']:
        if k not in ans:
            ans[k] = None         
    return ans                

def get_results_from_tags(combined_tags_dict,tokens):
    vc = ["phone_number","city","road","housenumber","postcode"]
    output_dict = {}
    results = []
    prev_token_k = -1
    for i,(k,v) in  enumerate(sorted(combined_tags_dict.items())):
        if prev_token_k != -1:
            between_string = " ".join(tokens[prev_token_k:k-1])
            print "Between String --> " , between_string
            between_string_tokens = between_string.replace("_", " ").split()
            if len(between_string_tokens) > 2:
                output_dict = {}
        
        output_dict[v[0]] = " ".join(tokens[k:k+v[2]])
               
        print "Amit-->",output_dict
        if valid_config(output_dict,vc):
            results.append(map_dict(output_dict))
            output_dict = {}
        
        prev_token_k = k + v[2]
        
    return results        


def make_comparator(less_than):
    def compare(x, y):
        if less_than(x, y):
            return -1
        elif less_than(y, x):
            return 1
        else:
            return 0
    return compare

def compare_numerical(x_dict,y_dict,input_dict,keyv):
    if keyv in x_dict and keyv in y_dict and keyv in input_dict:
        if x_dict[keyv] == input_dict[keyv] and y_dict[keyv] != input_dict[keyv]:
            return "x"
        elif x_dict[keyv] != input_dict[keyv] and y_dict[keyv] == input_dict[keyv]:
            return "y"
    return False        

def compare_string(x_dict,y_dict,input_dict,keyv):
    if keyv in x_dict and keyv in y_dict and keyv in input_dict:
            ed1 = edit_distance(x_dict[keyv],input_dict[keyv])
            ed2 = edit_distance(y_dict[keyv],input_dict[keyv])
            if ed1 < ed2:
                return "x"
            elif ed2 < ed1:
                return "y"
    return False 

class result:
  input_dict = {}
  def __init__(self,input):
    self.input_dict = input
  
  def less_than(self,x,y):
      x_dict = x['properties']['address']
      y_dict = y['properties']['address']
      pc = compare_numerical(x_dict,y_dict,input_dict,'postcode')
      if pc:
          if pc == "x":
            return True
          else:
              return False
      pc = compare_string(x_dict,y_dict,input_dict,'city')
      if pc:
          if pc == "x":
            return True
          else:
              return False
      pc = compare_string(x_dict,y_dict,input_dict,'street')
      if pc:
          if pc == "x":
            return True
          else:
              return False
      pc = compare_numerical(x_dict,y_dict,input_dict,'housenumber')
      if pc:
          if pc == "x":
            return True
          else:
              return False
      return True
      
      
def rank_results(geocode_output,input_dict):
    r = result(input_dict)
    sorted_geocode_output = sorted(geocode_output,cmp=make_comparator(r.less_than))
    return sorted_geocode_output
    
    
                    
def get_information(text,index_searcher):
    global index_searcher_holder
    index_searcher_holder = index_searcher
    text = text.encode('utf-8')
    tokens = text.split()
    index_tags = tag_using_indices(tokens)
    #print text
    print index_tags
   # for i,t in enumerate(tokens):
   #     print str(i) + ":::" + t + "\n"
    unicode_decoded_text = convert_string_to_unicode(text)    
    reg_ex_tags = find_matches(unicode_decoded_text)
    #print "Reg Ex Tags -- > ", reg_ex_tags
    
    combined_tags_dict = combine_tags(tokens,index_tags,reg_ex_tags)
    for k,v in sorted(combined_tags_dict.items()):
        if v[1] == 'i' and v[0] != 'housenumber':
            print "Index Match -->" + str(k) + " " + str(v) + " " + tokens[k]
    for k,v in sorted(combined_tags_dict.items()):
        if v[1] == 'r' and v[0] != 'housenumber':
            print "Regex Match -->" + str(k) + " " + str(v) + " " + tokens[k]
          
    results = get_results_from_tags(combined_tags_dict,tokens)
    output_results = []
   
    uniq_dict = {} 
    for i,dictv in enumerate(results):
         
         outputd = copy.deepcopy(default_dict)
         print "#"
         print "#"
         print "######################Geociding --> " , dictv
         print "#" 
         print "#"
        
         dictv['country'] = "Switzerland"
         gout = geocode(dictv)
         vs = []
         
	 if (gout):
	     vs = gout[0]['bbox']
             print gout[0]
             lat = (vs[0] + vs[2])/2
             lon = (vs[1] + vs[3])/2
             nlat,nlon = MetersToLatLon(lat, lon)
             outputd["Place"]["lat"] = nlat
             outputd["Place"]["lon"] = nlon
             for x in ["housenumber", "city","road","postcode","phone_number"]:
		 if x in dictv:
                     outputd["Place"][x] = dictv[x].decode('utf-8')
   
             output_results.append(outputd)
             print output_results
        
             

    center_lat, center_lon = compute_center(output_results)
    return {
        "center" : {
            "lat" : center_lat,
            "lon" : center_lon
        },
        "data" : output_results
    }





def compute_center(data):
  lats = []
  lons = []
  for result in data:
    if math.isnan(result['Place']['lat']) or math.isnan(result['Place']['lon']):
	continue
    lats.append(result['Place']['lat'])
    lons.append(result['Place']['lon'])
  return median(lats), median(lons)
  
  
if __name__ == '__main__':  
   pass 
